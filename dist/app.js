"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const express_session_1 = __importDefault(require("express-session"));
const connect_redis_1 = __importDefault(require("connect-redis"));
const type_graphql_1 = require("type-graphql");
const apollo_server_express_1 = require("apollo-server-express");
const redisConfigration_1 = require("./redisConfigration");
const _config_1 = __importDefault(require("@config"));
const core_1 = require("@mikro-orm/core");
const user_resolver_1 = __importDefault(require("@features/User/user.resolver"));
const orm_config_1 = __importDefault(require("./orm.config"));
class App {
    constructor() {
        this.app = express_1.default();
        this.redisStore = connect_redis_1.default(express_session_1.default);
        (() => __awaiter(this, void 0, void 0, function* () {
            this.orm = yield core_1.MikroORM.init(orm_config_1.default);
            const migrator = this.orm.getMigrator();
            const migrations = yield migrator.getPendingMigrations();
            if (migrations && migrations.length > 0) {
                yield migrator.up();
            }
            try {
                const schema = yield type_graphql_1.buildSchema({
                    resolvers: [user_resolver_1.default],
                    validate: true,
                });
                this.apolloServer = new apollo_server_express_1.ApolloServer({
                    schema,
                    uploads: false,
                    context: ({ req }) => ({
                        req,
                        em: this.orm.em.fork(),
                    }),
                });
                this.config();
            }
            catch (error) {
                console.log(error);
            }
        }))();
    }
    set app(app) {
        this._app = app;
    }
    get app() {
        return this._app;
    }
    config() {
        this.app.use('/media', express_1.default.static('../media'));
        const sessionOption = {
            store: new this.redisStore({
                client: redisConfigration_1.redis,
            }),
            name: 'qid',
            secret: _config_1.default.sessionSecret || '',
            saveUninitialized: false,
            cookie: {
                httpOnly: true,
                secure: process.env.NODE_ENV === 'production',
                maxAge: 1000 * 60 * 60 * 24 * 7 * 365,
            },
        };
        this._app.use(express_session_1.default(sessionOption));
        this.apolloServer.applyMiddleware({
            app: this.app,
            path: '/graphql',
        });
    }
}
exports.default = new App().app;
//# sourceMappingURL=app.js.map