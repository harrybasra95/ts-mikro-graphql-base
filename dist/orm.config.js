"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@mikro-orm/core");
const _config_1 = __importDefault(require("@config"));
exports.default = {
    migrations: {
        path: './src/migrations',
        tableName: 'migrations',
        pattern: /^[\w-]+\d+\.[t]s$/,
        transactional: true,
    },
    namingStrategy: core_1.EntityCaseNamingStrategy,
    tsNode: process.env.NODE_ENV === 'development' ? true : false,
    user: _config_1.default.db.username,
    password: _config_1.default.db.password,
    dbName: _config_1.default.db.database,
    host: _config_1.default.db.host,
    port: _config_1.default.db.port,
    type: _config_1.default.db.type,
    entities: ['dist/**/*.entity.js'],
    entitiesTs: ['src/**/*.entity.ts'],
};
//# sourceMappingURL=orm.config.js.map