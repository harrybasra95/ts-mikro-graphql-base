"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("http");
const app_1 = __importDefault(require("./app"));
const config_1 = __importDefault(require("./config/config"));
const PORT = config_1.default.PORT;
const httpServer = http_1.createServer(app_1.default);
httpServer.listen({ port: PORT }, () => console.log('Server started at ', PORT));
//# sourceMappingURL=index.js.map