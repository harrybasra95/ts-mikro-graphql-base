import { Migration } from '@mikro-orm/migrations';

export class Migration20210717035947 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `users` (`id` varchar(255) not null, `firstName` varchar(255) not null, `lastName` varchar(255) not null, `email` varchar(255) not null, `password` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `users` add primary key `users_pkey`(`id`);');
  }

}
