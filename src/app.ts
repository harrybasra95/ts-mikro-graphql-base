import express from 'express';
import session from 'express-session';
import connectRedis from 'connect-redis';
import { buildSchema } from 'type-graphql';
import { ApolloServer } from 'apollo-server-express';
import { redis } from './redisConfigration';
import config from '@config';
import { MikroORM } from '@mikro-orm/core';
import ormConfig from './orm.config';
import UserResolver from '@resolvers/user.resolver';

class App {
     private _app: express.Application;

     set app(app: express.Application) {
          this._app = app;
     }

     get app(): express.Application {
          return this._app;
     }

     private apolloServer: any;
     private redisStore: any;
     private orm: any;

     constructor() {
          this.app = express();
          this.redisStore = connectRedis(session);
          (async () => {
               this.orm = await MikroORM.init(ormConfig);
               const migrator = this.orm.getMigrator();
               const migrations = await migrator.getPendingMigrations();
               if (migrations && migrations.length > 0) {
                    await migrator.up();
               }
               try {
                    const schema = await buildSchema({
                         resolvers: [UserResolver],
                         validate: true,
                    });
                    this.apolloServer = new ApolloServer({
                         schema,
                         uploads: false,
                         context: ({ req }: any) => ({
                              req,
                              em: this.orm.em.fork(),
                         }),
                    });
                    this.config();
               } catch (error) {
                    console.log(error);
               }
          })();
     }

     private config(): void {
          this.app.use('/media', express.static('../media'));
          const sessionOption: session.SessionOptions = {
               store: new this.redisStore({
                    client: redis,
               }),
               name: 'qid',
               secret: config.sessionSecret || '',
               saveUninitialized: false,
               cookie: {
                    httpOnly: true,
                    secure: process.env.NODE_ENV === 'production',
                    maxAge: 1000 * 60 * 60 * 24 * 7 * 365, //7 years
               },
          };
          this._app.use(session(sessionOption));
          this.apolloServer.applyMiddleware({
               app: this.app,
               path: '/graphql',
          });
     }
}

export default new App().app;
