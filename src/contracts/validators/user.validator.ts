import { IsEmail, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class UserValidator {
     @Field()
     @IsString()
     firstName: string;

     @Field()
     @IsString()
     lastName: string;

     @Field()
     @IsEmail()
     email: string;

     @Field()
     @IsString()
     password: string;
}
