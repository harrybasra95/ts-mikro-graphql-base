import Base from '@baseEntity';
import { Entity, Property } from '@mikro-orm/core';
import UserValidator from '@validators/user.validator';
import { Field, ObjectType } from 'type-graphql';

@Entity({ tableName: 'users' })
@ObjectType({ description: 'The User model' })
export default class User extends Base<User> {
     @Field(() => String, { description: 'This is the first name of the user' })
     @Property()
     firstName: string;

     @Field(() => String, { description: 'This is the last name of the user' })
     @Property()
     lastName: string;

     @Field(() => String, { description: 'This is the email of the user' })
     @Property()
     email: string;

     @Property()
     password: string;

     constructor(body: UserValidator) {
          super(body);
     }
}
