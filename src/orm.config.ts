import { MikroORM, EntityCaseNamingStrategy } from '@mikro-orm/core';
import config from '@config';

export default {
     migrations: {
          path: './src/migrations',
          tableName: 'migrations',
          pattern: /^[\w-]+\d+\.[t]s$/,
          transactional: true,
     },
     namingStrategy: EntityCaseNamingStrategy,
     tsNode: process.env.NODE_ENV === 'development' ? true : false,
     user: config.db.username,
     password: config.db.password,
     dbName: config.db.database,
     host: config.db.host,
     port: config.db.port,
     type: config.db.type,
     entities: ['dist/**/*.entity.js'],
     entitiesTs: ['src/**/*.entity.ts'],
} as Parameters<typeof MikroORM.init>[0];
