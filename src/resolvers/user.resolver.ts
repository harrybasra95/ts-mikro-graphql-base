import User from '@entities/user.entity';
import MyContext from '@interfaces/context.interface';
import UserValidator from '@validators/user.validator';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import { Arg, Ctx, Info, Mutation, Query, Resolver } from 'type-graphql';

@Resolver(User)
export default class UserResolver {
     @Query((_returns) => [User], {
          nullable: true,
          description: 'Get details of all the Users',
     })
     async users(
          @Ctx()
          ctx: MyContext,
          @Info()
          info: GraphQLResolveInfo
     ): Promise<[User]> {
          const relationPaths = fieldsToRelations(info);
          const foundUsers = await ctx.em
               .getRepository(User)
               .findAll(relationPaths);
          return foundUsers as [User];
     }

     @Mutation((_returns) => User, {
          description: 'Create user',
     })
     async createUser(
          @Ctx()
          ctx: MyContext,

          @Arg('data', (_type) => UserValidator)
          data: UserValidator
     ): Promise<User> {
          const user = new User(data);
          await ctx.em.persist(user).flush();
          return user;
     }
}
